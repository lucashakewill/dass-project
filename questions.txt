Stress

  I found it hard to wind down
  I tended to over-react to situations
  I felt that I was using a lot of nervous energy
  I found myself getting agitated
  I found it difficult to relax
  I was intolerant of anything that kept me from
  getting on with what I was doing
  I felt I was rather touchy
  
Anxiety

  I was aware of dryness of my mouth
  I experienced breathing difficulty (e.g. excessively rapid breathing,
  breathlessness in the absence of physical exertion)
  I experienced trembling (e.g. in the hands)
  I was worried about situations in which I might panic
  and make a fool of myself
  I felt I was close to panic
  I was aware of the action of my heart in the absence of physical
  exertion (e.g. sense of heart rate increase, heart missing a beat)
  I felt scared without any good reason


Depression

  I couldn't seem to experience any positive feeling at all
  I found it difficult to work up the initiative to do things
  I felt that I had nothing to look forward to
  I felt down-hearted and blue
  I was unable to become enthusiastic about anything
  I felt I wasn't worth much as a person
  I felt that life was meaningless
