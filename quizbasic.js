/* how many questions of each type are there */


function tally(){

  var radio = document.getElementsByTagName('input');
  var stressValue;
  for (var i = 0; i < radio.length; i++) {
      if (radio[i].type === 'radio' && radio[i].checked) {
          stressValue = radio[i].value;
      }
  }
  var text;
  if (stressValue === undefined)
    text = "Please select a value from the above checkboxes.";
  else
   text = "Your answer has a stress score of " + stressValue + ".";

  document.getElementById("answer").innerHTML = text;
}
